<?php

namespace Drupal\layout_builder_starterkit\Controller;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Core\Url;
use Drupal\layout_builder_restrictions\Controller\ChooseBlockController as ChooseBlockControllerCore;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Class ChooseBlockController
 *
 * Add inline block category and render block icons.
 */
class ChooseBlockController extends ChooseBlockControllerCore {

  /**
   * {@inheritdoc}
   */
  public function build(SectionStorageInterface $section_storage, $delta, $region) {
    $build = parent::build($section_storage, $delta, $region);

    // Close all others.
    foreach ($build['block_categories'] as $key => &$form_category) {
      if (!is_array($form_category)) {
        continue;
      }
      $form_category['#open'] = FALSE;
    }

    // Remove 'Create custom block' link.
    if (isset($build['add_block'])) {
      unset($build['add_block']);
    }

    // Get the blocks.
    $blocks = $this->inlineBlockList($section_storage, $delta, $region);
    unset($blocks['back_button']);

    // Add them in under a new 'Inline blocks' category.
    $name = (string) $this->t('Inline blocks');
    $build['block_categories'][$name] = [
      '#type' => 'details',
      '#attributes' => [
        'class' => [
          'js-layout-builder-category'
        ]
      ],
      '#open' => TRUE,
      '#title' => $name,
      '#weight' => -1,
      'links' => $blocks,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * todo: rework some of this markup stuff. Figure out what to do with
   * these default icons. Per category? Something else?
   */
  protected function getBlockLinks(SectionStorageInterface $section_storage, int $delta, $region, array $blocks) {
    $links = [];
    $icons_folder_path = drupal_get_path('module', 'layout_builder_starterkit') . '/assets/images/';
    $default_icon_path = file_create_url($icons_folder_path . 'default-block.svg');
    foreach ($blocks as $block_id => $block) {
      $description = '';
      $icon_path = $default_icon_path;
      if ($block['category'] == (string) $this->t('Inline blocks')) {
        /** @var \Drupal\block_content\BlockContentTypeInterface $block_type */
        $block_type = BlockContentType::load(str_replace('inline_block:', '', $block_id));
        if ($block_type) {
          if ($block_type->getThirdPartySetting('layout_builder_starterkit', 'icon_path', NULL)) {
            $icon_path = $block_type->getThirdPartySetting('layout_builder_starterkit', 'icon_path', NULL);
          }
          $description = '<p>' . $block_type->getDescription() . '</p>';
        }
      }
      if ($block['category'] == (string) $this->t('Custom')) {
        $l = $this->entityTypeManager->getStorage('block_content')
          ->loadByProperties(['uuid' => str_replace('block_content:', '', $block_id)]);
        $loaded_block = reset($l);
        if ($loaded_block) {
          $block_type = BlockContentType::load($loaded_block->bundle());
          if ($block_type) {
            if ($block_type->getThirdPartySetting('layout_builder_starterkit', 'icon_path', NULL)) {
              $icon_path = $block_type->getThirdPartySetting('layout_builder_starterkit', 'icon_path', NULL);
            }
            $description = '<p>' . $block_type->getDescription() . '</p>';
          }
        }
      }
      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-block-link';
      $link = [
        'title' => [
          'icon' => [
            '#theme' => 'image',
            '#uri' => $icon_path,
            '#alt' => $block['admin_label'],
          ],
          'title' => [
            '#markup' => '<div class="inline-block-list__item__title">' . $block['admin_label'] . '</div>',
          ],
          'description' => [
            '#markup' => '<div class="inline-block-list__item__descr">' . $description . '</div>',
          ],

        ],
        'url' => Url::fromRoute('layout_builder.add_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]
        ),
        'attributes' => $attributes,
      ];

      $links[] = $link;
    }
    return [
      '#theme' => 'links',
      '#links' => $links,
    ];
  }

}
